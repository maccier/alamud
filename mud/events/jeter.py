# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import  Event3



class JeterEvent(Event3):
    NAME = "jeter"

    def perform(self):
        if self.object2.has_prop("jetable"):
            return self.inform("jeter.failed")
        if self.object.has_prop("receptacle"):
            return self.inform("jeter.failed")
        self.object.move_to(self.actor.container())
        self.inform("jeter")